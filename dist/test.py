from flask import Flask,request,jsonify
import json
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
import pandas as pd

app=Flask(__name__)
@app.route('/', methods=['GET', 'POST'])
#@app.route('/receiver', methods = ['POST'])
def make_predict():
    data1=request.get_json(force=True)
    #print(data1['words'])
    l = data1['words']
    predictions={}
    print(l)
    data = pd.read_csv('/home/manisha/Documents/gsoc/toxic-nontoxic-words.csv',sep=',')
    Q_train, Q_test, T_train, T_test = train_test_split(data["words"],data["toxicity"], test_size=0.0, random_state=2)
    Q_train, Q_val, T_train, T_val  = train_test_split(Q_train, T_train, test_size=0.2, random_state=0)
    # Applying vectorizer
    vectorizer = CountVectorizer()
    vectorizedTrainQuestions = vectorizer.fit_transform(Q_train)
    encodedTestQuestions = vectorizer.transform(l)
    model = LogisticRegression(multi_class='ovr', solver='liblinear')
    # Fitting the data
    model = model.fit(vectorizedTrainQuestions,T_train)
    # Prediction using test data
    predicted = model.predict(encodedTestQuestions)
    #output=np.array(predicted).tolist()
    output=np.array(list(zip(l,predicted))).tolist()
    print(output)
    for i in range(len(output)):
        predictions[output[i][0]]=output[i][1]
    print(predictions)
    print("-------------------------------------------------------")
    return jsonify(predictions)
   

if __name__ == '__main__':
      app.run()
